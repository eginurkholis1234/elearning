<?php

namespace app\controllers;

use Yii;
use app\models\Item;
use app\models\ItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



public function actionReport(){
		$model = new Item();
		$items = null;
		
		if ($model->load(Yii::$app->request->post())) {
			$items = Item::find()
		->where([
			'status' => $model->status
			])
			->all(); //menampilkan semua data
	}
	
	return $this->render('report',
	[
	'model' =>$model,
	'items' => $items
	]
	);
	}
public function actionAddStock($id){
	$model = $this-> findModel($id);
	$modelIncomingItemHistory = new incomingItemHistory();
	
	if($model->load(Yii::$app->request->post()) && $modelIncomingItemHistory->load(yii::$app->request->post())){
			$trans = \Yii::$app->db->beginTransaction();
			$saved = TRUE;
			$message="Data Berhasil Disimpan";
			
			$modelIncomingItemHistory->item_id = $model->id;
			$modelIncomingItemHistory->date ('Y-m-d');
			$saved = $saved && $modelIncomingItemHistory->dave();
			
			if($sved) {
				$model->qty = $modelIncomingItemHistory->amount + $model->qty;
				
				$save = $save && $model->save();
				
				if(!$saved) {
					$messages = "gagal merubah stok, error :";
				}
			}else {
					$messages = "gagal menambah data history";
				}
				
				if($saved) {
					$trans->commit();
					return $this->redirect(['view','id'=> $model->id]);
				} else {
					\Yii::$app->session->setFlash("error",$message);
					$trans->rollBack();
				}
				
			}
			return $this->render('add_stock',[
			'model' => $model,
			'modelIncomingItemHistory' => $modelIncomingItemHistory
			]);
	}
}