<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incoming_item_history".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $supplier_id
 * @property integer $amount
 * @property string $date
 */
class IncomingItemHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incoming_item_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'supplier_id', 'amount'], 'required'],
            [['item_id', 'supplier_id', 'amount'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'supplier_id' => 'Supplier ID',
            'amount' => 'Amount',
            'date' => 'Date',
			        ];
	}
			public function getListSupplier() {
				$supplier = Supplier::find()->orderBy('name')->all();
				$listData = ArrayHelper::map($suppliers, 'id', 'name');
				return $listData;

    }
}
