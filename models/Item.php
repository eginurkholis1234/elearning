<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $name
 * @property integer $qty
 * @property string $price
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['qty'], 'integer'],
            [['price'], 'number'],
			[['status'], 'string'],
            [['name'], 'string', 'max' => 255],
			
			[
			[
				'name',
				'status',
				
			],
			'string',
			'max' => 255
		],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'qty' => 'Qty',
            'price' => 'Price',
			'status' => 'Status',
        ];
    }
}
