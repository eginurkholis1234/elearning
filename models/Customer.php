<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $name
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
		
			return [
		[
			[
				'name',
				'gender',
				'address',
				'phone',
				'identity_number',
			],
			'required'
		],
		[
			[
				'name',
				'gender',
				'address',
				'birth_place',
			],
			'string',
			'max' => 255
		],
		[
			[
			'phone'
		],
				'string',
				'length' => [7, 13],
				'message' => "panjang Nomor Telpon 7-13 Karakter",
		],
		
		[
			[
			'identity_number'
		],
				'string',
				'length' => [16],
				'message' => "panjang identity_number 16 Karakter",
		],
		
		[
			[
			'phone',
			'identity_number'
		],
				'integer',
				'message' => "[attribute] Hanya Boleh Diisi Angka"
		],
		[
			[
			'birth_date',
		],
				'date',
				'format' => 'php:Y-m-d'
		],
	];
	}

  public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'gender' => 'Gender',
            'address' => 'Address',
            'phone' => 'Phone',
		
        ];
    }
}