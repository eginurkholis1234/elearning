<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
	$genders = [
	'Pria' => 'Pria',
	'Wanita' => 'Wanita'
	];
?>


<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'gender')->dropDownList($genders)
 ?>
<?= $form->field($model, 'address')->textarea(['maxlength' =>true]) ?>
<?= $form->field($model, 'birth_date')->textInput() ?>
<?php
	DatePicker::widget([
	'model'=> $model,
	'attribute'=> 'birth_date',
	'dateFormat'=>'yyyy-MM-dd',
	'clientOptions'=>[
		'changeYear' > true,
		'changeMonth'> true,
		],
		]);
?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
