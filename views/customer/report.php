<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



$this->title ='Pencarian Data';
$this->params['breadcrumbs'][] = ['label' =>'Pelanggan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-report">

	<h1><?= Html::encode($this->title) ?></h1>
	
	<div class="customer-form">
		
		<?php $form =ActiveForm::begin(); ?>
		<?php
			$genders= [
			'Pria' => 'Pria',
			'Wanita' => 'Wanita'
			];
			?>
			<?= $form->field($model, 'gender')->dropDownList($genders)?>
			
			<div  class"form-group">
			<?= Html::submitButton('cari' , ['class' => 'btn btn-primary']) ?>
			</div>
			
			<?php ActiveForm::end();?>
			
			</div> 
			
			<?php if($customers) : ?>
			<?php $nomor = 1; ?>
			<table class="table table-striped">
			<thead>
			<tr>
			<th class="text-center">No</th>
			<th class="text-center">Nanma</th>			
			<th class="text-center">Jenis Kelamin</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($customers as $customer) :?>
			<tr>
			<td class="text-center"><?= $nomor++ ?></td>
			<td><?= $customer->name?></td>
			<td><?= $customer->gender?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
			</table>
			<?php endif; ?>
			</div>