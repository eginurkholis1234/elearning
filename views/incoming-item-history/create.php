<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Incomingitemhistory */

$this->title = 'Create Incomingitemhistory';
$this->params['breadcrumbs'][] = ['label' => 'Incomingitemhistories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incomingitemhistory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
