<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Incomingitemhistory */

$this->title = 'Update Incomingitemhistory: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Incomingitemhistories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="incomingitemhistory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
