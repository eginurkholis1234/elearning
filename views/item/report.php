<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



$this->title ='Pencarian Data';
$this->params['breadcrumbs'][] = ['label' =>'Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-report">

	<h1><?= Html::encode($this->title) ?></h1>
	
	<div class="item-form">
		
		<?php $form =ActiveForm::begin(); ?>
		<?php
			$pilihan = [
				'Tersedia' => 'Tersedia',
					'Habis' => 'Habis'
			];
		?>
			<?= $form->field($model, 'status')->dropDownList($pilihan) ?>
			
			<div  class"form-group">
			<?= Html::submitButton('cari' , ['class' => 'btn btn-primary']) ?>
			</div>
			
			<?php ActiveForm::end();?>
			
			</div> 
			
			<?php if($items) : ?>
			<?php $nomor = 1; ?>
			<table class="table table-striped">
			<thead>
			<tr>
			<th class="text-center">No</th>
			<th class="text-center">Nama Barang</th>			
			<th class="text-center">Satus</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($items as $item) :?>
			<tr>
			<td class="text-center"><?= $nomor++ ?></td>
			<td><?= $item->name?></td>
			<td><?= $item->status?></td>
			</tr>
			<?php endforeach; ?>
			</tbody>
			</table>
			<?php endif; ?>
			</div>