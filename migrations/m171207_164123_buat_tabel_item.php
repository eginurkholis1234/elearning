<?php

use yii\db\Migration;

class m171207_164123_buat_tabel_item extends Migration
{
	private $_tableName = "item";
    public function safeUp()
    {
$columns = [
		'id'		=>$this->primaryKey(),
		'name'		=>$this->string()->notnull(),
		'qty'		=>$this->integer()->defaultValue(0),
		'price'		=>$this->decimal()->defaultValue(0),
		'status'	=>$this->string()->notnull()
		];
		return $this->createTable($this->_tableName, $columns);

    }
    

    public function safeDown()
    {
        		return $this->dropTable($this->_tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_164123_buat_tabel_item cannot be reverted.\n";

        return false;
    }
    */
}
