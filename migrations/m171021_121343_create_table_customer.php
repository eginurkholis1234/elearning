<?php

use yii\db\Migration;

class m171021_121343_create_table_customer extends Migration
{
	private $_tableName = "customer";
	
    public function safeUp()
    {
	$columns = [
		'id'	=>$this->primaryKey(),
		'name'	=>$this->string()->notnull(),
		'gender'	=>$this->string()->notnull(),
		'address'	=>$this->string()->notnull(),
		'phone'	=>$this->string()->notnull(),
		'identity_number'	=>$this->string()->notnull(),
		'birth_place'	=>$this->string(),
		'birth_date'	=>$this->date()
		];
		return $this->createTable($this->_tableName, $columns);
    }

    public function safeDown()
    {
       return $this->dropTable($this->_tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171021_121343_create_table_customer cannot be reverted.\n";

        return false;
    }
    */
}
