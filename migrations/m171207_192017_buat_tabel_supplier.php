<?php

use yii\db\Migration;

class m171207_192017_buat_tabel_supplier extends Migration
{
	private $_tableName = "supplier";
    public function safeUp()
    {
	$columns = [
		'id'		=>$this->primaryKey(),
		'name'		=>$this->string()->notnull(),
		'gender'	=>$this->string()->notnull(),
		'address'	=>$this->string()->notnull(),
		
		];
		return $this->createTable($this->_tableName, $columns);
    
    }

    public function safeDown()
    {
        return $this->dropTable($this->_tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_192017_buat_tabel_supplier cannot be reverted.\n";

        return false;
    }
    */
}
