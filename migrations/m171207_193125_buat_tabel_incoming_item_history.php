<?php

use yii\db\Migration;

class m171207_193125_buat_tabel_incoming_item_history extends Migration
{
	private $_tableName = "incoming_item_history";
    public function safeUp()
    {
	$columns = [
		'id'	=>$this->primaryKey(),
		'item_id'	=>$this->integer()->notnull(),
		'supplier_id'	=>$this->integer()->notnull(),
		'amount'	=>$this->integer()->notnull(),
		'date'	=>$this->date(),
		
		];
		return $this->createTable($this->_tableName, $columns);
    }

    public function safeDown()
    {
        return $this->dropTable($this->_tableName);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_193125_buat_tabel_incoming_item_history cannot be reverted.\n";

        return false;
    }
    */
}
